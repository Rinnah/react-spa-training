import React from 'react';

import Hero from '../components/Hero';
import News from '../components/News';

function Top(props)  {
  return (
    <main>
      <Hero sliderWidth="1440" sliderHeight="667" />
      <News LoggedIn={props.LoggedIn} />
    </main>
  )
}

export default Top;
