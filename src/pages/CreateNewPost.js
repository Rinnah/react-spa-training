import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import Breadcrumbs from '../components/Breadcrumbs';
import ImageUploader from "react-images-upload";

function CreateNewPost(props) {

  const [pictures, setPictures] = useState([]);

  const handleUpload = (pictureFiles, pictureDataURLs) => {
    setPictures(pictures.concat(pictureFiles))
  }

  const history = useHistory();

  const handleCancelClick = () => {
    window.alert('Discard Changes?');
    history.push('/');
  }

  const handleSaveClick = () => {
    history.push('/news/news-detail');
  }

  const link = 'Create New Post';
  const today = new Date(),
        date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

  return (
    <main className="new-post">
      <Breadcrumbs link={link}/>

      <div className="l-container">
        <div className="new-post-header">
          <button className="new-post-btn" onClick={handleSaveClick}>Save Post</button>
          <button className="new-post-btn" onClick={handleCancelClick}>Cancel</button>
        </div>

        <div className="new-post-form">
          <form>
            <time className="new-post-date">{date}</time>
            <textarea placeholder="Title" className="new-post-textarea"></textarea>
            <ImageUploader
            withIcon={false}
            withPreview={true}
            label=""
            buttonText="Upload Image"
            onChange={props.handleUpload}
            imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
            maxFileSize={1114264}
            fileSizeError=" file size is too big"
          />
          <textarea className="new-post-content" placeholder="Content">
          </textarea>
          </form>
        </div>
      </div>
    </main>
  )

}

export default CreateNewPost;
