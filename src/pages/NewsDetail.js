import React, { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';
import ImageUploader from "react-images-upload";

import Comment from '../components/Comment';
import Breadcrumbs from '../components/Breadcrumbs';

function NewsDetail(props) {

  const [ editPost, setEditPost ] = useState(false);
  const [pictures, setPictures] = useState([]);

  const handleUpload = (pictureFiles, pictureDataURLs) => {
    setPictures(pictures.concat(pictureFiles))
  }

  const handleEditClick = () => {
    setEditPost(true);
  }

  const handleCancelClick = () => {
    window.alert('Discard Changes?');
    setEditPost(false);
  }

  const handleSaveClick = () => {
    setEditPost(false);
  }

  const handleEditPost = <div className="new-post-header">
    <button className="new-post-btn" onClick={handleEditClick}>Edit Post</button>
  </div>

  const handleSavePost = <div className="new-post-header">
    <button className="new-post-btn" onClick={handleSaveClick}>Save Post</button>
    <button className="new-post-btn" onClick={handleCancelClick}>Cancel</button>
  </div>

  const detail = {
    date:'2019.06.19',
    img:'/assets/images/hero-img.png',
    title:'サンプルテキストサンプル ルテキストサンプルテキストサンプルテキストサンプル ルテキスト ',
    content: '<p>これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです</p><p>これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです</p>'
  };

  const LoggedIn = props.LoggedIn;

  return (
    <main className="news-detail">
      <Breadcrumbs link={detail.title}/>

      <div className="l-container">

      <div className="new-post-header">
       { LoggedIn ? (editPost ? handleSavePost : handleEditPost) : <span className="new-post-btn"></span> }
      </div>


        <div className="new-detail-form">
          <form>
            <time className="new-post-date">{detail.date}</time>
            { editPost ?
              <textarea placeholder="Title" className="new-post-textarea" defaultValue={detail.title}></textarea>
              : <h1 className="news-detail-title">{detail.title}</h1>
            }

            { editPost ?
              <div className="news-detail-img">
                <ImageUploader
                  withIcon={false}
                  withPreview={true}
                  label=""
                  buttonText="Upload Image"
                  onChange={props.handleUpload}
                  imgExtension={[".jpg", ".gif", ".png", ".gif", ".svg"]}
                  maxFileSize={1114264}
                  fileSizeError=" file size is too big" />
              </div>
              : <div className="news-detail-img">
                <img src={detail.img} alt={detail.title} />
              </div>
            }

            { editPost ?
              <textarea className="new-post-content" defaultValue={detail.content} placeholder="Content">
              </textarea>
            : <div className="news-detail-content">
                {ReactHtmlParser(detail.content)}
              </div>
            }
          </form>
        </div>

        <Comment LoggedIn={props.LoggedIn} />

      </div>
    </main>
  )
}

 export default NewsDetail;
