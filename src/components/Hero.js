import React, { Component } from "react";

export default class Hero extends Component {
  constructor(props) {
    super(props)

    this.state = {
      slider: [
        "/assets/images/hero-img.png",
        "/assets/images/hero-img.png",
        "/assets/images/hero-img.png"
      ],
      activeIndex: 1,
      left: 0
    }
  }

  prevSlide = () => {
    this.setState({
      activeIndex: this.state.activeIndex - 1,
      left: this.state.left + 1440 // this.props.sliderWidth not working for some reason
    })
    if (this.state.activeIndex === 1) {
      this.setState({
        activeIndex: this.state.activeIndex + this.state.slider.length - 1,
        left: this.state.left - this.props.sliderWidth * (this.state.slider.length - 1)
      })
    }
  }

  nextSlide = () => {
    this.setState({
      activeIndex: this.state.activeIndex + 1,
      left: this.state.left - this.props.sliderWidth
    })
    if (this.state.activeIndex === this.state.slider.length) {
      this.setState({
        activeIndex: this.state.activeIndex - this.state.slider.length + 1,
        left: 0
      })
    }
  }

  clickIndicator = e => {
    this.setState({
      activeIndex: parseInt(e.target.textContent),
      left: this.props.sliderWidth - parseInt(e.target.textContent) * this.props.sliderWidth
    })
  }

  render() {
    const style = {
      left: this.state.left,
      width: this.props.sliderWidth,
      height: this.props.sliderHeight
    };

    return (
      <div className="slider-container">
        <div  className="slider-wrapper">
          <ul className="slider">
          {
            this.state.slider.map((item,index) => (
            <li style={style} key={index} className={index+1 === this.state.activeIndex ? 'slider-item' : 'hide'}>
              <img src={item} alt="slider image" />
              <div className="slide-content">
                <div className="slide-heading"><span className="slide-text">サンプルテキスト</span></div>
                <div className="slide-heading"><span className="slide-text">サンプルルテキスト</span></div>
                <div className="slide-heading last"><span className="slide-text">サンプルテキスト</span></div>
                <time className="slide-date">2019.06.19</time>
              </div>
            </li>
            ))
          }
          </ul>
        </div>
        <div className="buttons-wrapper">
          <button className="prev-button" onClick={this.prevSlide}></button>
          <button className="next-button" onClick={this.nextSlide}></button>
        </div>
        <div className="indicators-wrapper">
          <ul className="indicators">
         {
           this.state.slider.map((item,index) => (

          <li key={index} className={index+1 === this.state.activeIndex ? 'active-indicator' : ''}onClick={this.clickIndicator}>{index+1}</li>
            ))
          }
          </ul>
        </div>
      </div>
      );
    }
  }
