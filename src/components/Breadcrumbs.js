import React from "react";
import { Link } from "react-router-dom";

function Breadcrumbs(props) {

    let link = props.link;

		return (
      <section className="breadcrumbs">
        <div className="l-container">
          <ul className="breadcrumbs-item">
            <li><Link to="/">HOME</Link></li>
            <li>{link}</li>
          </ul>
        </div>
      </section>
		)

}

export default Breadcrumbs;
