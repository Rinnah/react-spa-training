import React from 'react';
import { Link } from "react-router-dom";
import posts from "./postsArray";

const Articles = (props) => {
  const id = props.id;
  const {img, date, title} = props.item;

  const scrollTop = () => {
     window.scrollTo({top: 0, behavior: 'smooth'});
  };

  return(
    <li key={`posts-${id}`} className="news-item">
      <article>
        <Link to={`/news/${title}`} className="news-item-link" onClick={scrollTop}>
          <div className="news-image-con">
            <img src={img} alt={img} className="news-image"/>
          </div>
          <div className="news-content">
            <time className="news-date">{date}</time>
            <h3 className="news-title">
              {title}
            </h3>
          </div>
        </Link>
      </article>
    </li>
  )
}
export default Articles;
