import React, { Component } from 'react';

export default class Comment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showComments: true,
      comments: [
        {
          id: 1,
          body:'これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです',
          date:'3 months ago'
        },
        {
          id: 2,
          body:'これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです これはダミーテキストですこれはダミーテキストですこれはダミーテキストですこれはダミーテキストです',
          date:'3 months ago'
        }
      ]
    };
  }

  render () {
    const comments = this._getComments();
    let commentNodes;

    if (this.state.showComments) {
      commentNodes = <div className="comment-list">{comments}</div>;
    }
    return (
      <section className="comment">
        <h2 className="comment-heading">COMMENT</h2>

        {commentNodes}

        <CommentForm addComment={this._addComment.bind(this)}/>


      </section>
    )
  }

  _addComment(body, date) {
    const comment = {
      id: this.state.comments.length + 1,
      body,
      date
    };
    this.setState({ comments: this.state.comments.concat([comment]) }); // *new array references help React stay fast, so concat works better than push here.
  }

  _getComments() {
    return this.state.comments.map((comment) => {
      return (
        <Comments
          date={comment.date}
          body={comment.body}
          key={comment.id} />
      );
    });
  }

  _getCommentsTitle(commentCount) {
    if (commentCount === 0) {
      return 'No comments yet';
    } else if (commentCount === 1) {
      return "1 comment";
    } else {
      return `${commentCount} comments`;
    }
  }

}

class CommentForm extends React.Component {
  render() {
    return (
      <form className="comment-form" onSubmit={this._handleSubmit.bind(this)}>
        <div className="comment-form-fields">
          <textarea className="comment-textarea" placeholder="Comment" rows="4" required ref={(textarea) => this._body = textarea}></textarea>
        </div>
        <div className="comment-form-actions">
          <button className="comment-button" type="submit">Submit</button>
        </div>
      </form>
    );
  } // end render

  _handleSubmit(event) {
    event.preventDefault();   // prevents page from reloading on submit
    let body = this._body;
    this.props.addComment(body.value);
  }
} // end CommentForm component

class Comments extends React.Component {
  constructor(props) {
    super(props);
    const today = new Date(),
    date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    this.state = {
      currentDate: date
    }

  }
  render () {

    const LoggedIn = this.props.LoggedIn;

    return(
      <div className="comment-content">
        <p className="comment-body">{this.props.body}</p>
        <time className="comment-date">{this.props.date}</time>

         { LoggedIn ?
           <div className="comment-footer">
             <a href="#" className="comment-footer-delete" onClick={this._deleteComment}>Delete Comment</a>
           </div>
          :  null }

      </div>
    );
  }
  _deleteComment() {
    alert("-- DELETE Comment Functionality COMMING SOON...");
  }
}
