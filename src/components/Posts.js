import React from "react";
import { Link } from 'react-router-dom';

import Articles from '../components/Articles';

const Posts = ({ postsToRender }) => {

  return (

    <ul className="news-ul">
      {postsToRender.map((post, id) => (
        <Articles item={post} id={id} />
      ))}
    </ul>
  );
};

export default Posts;
