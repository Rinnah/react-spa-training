import React, { Component } from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.scss';
import './style/style.scss';

// Pages
import Header from './layout/Header.js'
import Main from './layout/Main.js'
import Footer from './layout/Footer.js'

class App extends Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.state = { LoggedIn: false};
  }

  handleLogout() {
    this.setState({LoggedIn: false});
  }

  handleLogin() {
    this.setState({LoggedIn: true});
  }

  render() {
    return (
        <div className="App">
          <BrowserRouter>
            <Header
            LoggedIn={this.state.LoggedIn}
            handleLogout={this.handleLogout}
            handleLogin={this.handleLogin}
            />
            <Main LoggedIn={this.state.LoggedIn}/>
            <Footer />
          </BrowserRouter>
        </div>
      )
  }
}

export default App;
