import React from 'react';
import { Link } from 'react-router-dom';

function Footer() {

  const scrollTop = () => {
     window.scrollTo({top: 0, behavior: 'smooth'});
  };

  return (
    <footer>
      <div className="footer">
        <div className="l-container footer-container">
          <Link to="/"><div className="footer-logo"></div></Link>
          <div className="footer-content">
            <p>サンプルテキストサンプル ルテキストサンプルテキストサ<br />
            ンプルテキストサンプル ルテキスト</p>
          </div>

          <div className="scroll-top">
            <button onClick={scrollTop}  className="scroll-btn"><i className="scroll-arrow"></i>TOP</button>
          </div>

        </div>
      </div>
      <div className="copyright">
          <p>Copyright©2007-2019 Blog Inc.</p>
      </div>
    </footer>
  )
}

 export default Footer;
