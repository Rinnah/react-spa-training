import React from 'react';
import { Link } from 'react-router-dom';

import Login from '../components/Login';

function Header(props) {
    return (
      <header className="header">
          <div className="l-container">
            <div className="header-container">
              <Link to="/"><div className="main-logo"></div></Link>
              <Login LoggedIn={props.LoggedIn} handleLogin={props.handleLogin} handleLogout={props.handleLogout}/>
            </div>
          </div>
      </header>
    )
}

export default Header;
